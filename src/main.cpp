#include <iostream>
#include <string>

int main()
{
    // Configurations
    int deck[104];
    int i;
    std::string suit_values[4]={"Spades", "Diamons", "Clubs", "Hearths"};
    std::string card_values[26]={"2", "2", "3", "3", "4", "4", "5", "5", "6", "6", "7", "7", "8", "8", "9", "9", "10", "10", "J", "J", "Q", "Q", "K", "K", "A", "A"};
    srand(time(0));
    // Creating a new deck
    for(i=0; i<104; i++)
    {
        deck[i] = i;
    }

    // Shuffling the deck with temporary variable
    for(i=0; i<104; i++)
    {
        int j = rand() % 104;
        std::swap(deck[i], deck[j]);
    }

    // Getting values and suits of the five first cards
    int card[5]; 
    int suit[5];
    for(i=0; i<5; i++)
    {
        card[i] = deck[i]%26;
        suit[i] = deck[i]/26;
    }
    // Swapping cards to ascending order to make this assigment easier
    bool ascending_order = false;
    do
    {
        ascending_order = false;
        for(int i=0; i<4; i++)
        {
            if(card[i] > card[i+1])
            {
                std::swap(card[i], card[i+1]);
                ascending_order = true;
            }
        }
    }
    // Main loop is made with while loop
    while(ascending_order == true);
    {
        // This for loop tells the cards
        for(i=0; i<5; i++)
        {
            std::cout << card_values[card[i]] << " of " << suit_values[suit[i]]<< "\n";
            // Because this program haves same string values with different int values like "2" and "2" with 0 and 1 values
            // This if statement makes odd values to same as its string even values because it helps making definitions to poker hand
            if(card[i] % 2)
            {
                card[i] = card[i] - 1;
            }
        }
        // 1.Five of the kind definitions
        if(card[0] == card[1] && card[1] == card[2] && card[2] == card[3] && card[3] == card[4])
        {
            std::cout << "*Five of tke kind*" << std::endl;
        }
        // 2.Straight flush definitions
        else if(suit[0] == suit[1] && suit[1]==suit[2] && suit[2]==suit[3] && suit[3]==suit[4] && card[0]==card[1]-2 && card[1]==card[2]-2 && card[2]==card[3]-2 && card[3]==card[4]-2)
        {
        
            std::cout << "*Straight flush*" << std::endl;
        
        }
        // 3.Four of a kind definitions
        else if(card[0] == card[1] && card[1] == card[2] && card[2] == card[3] || card[1] == card[2] && card[2] == card[3] && card[3] == card[4] || card[2] == card[3] && card[3] == card[4] && card[4] == card[0] ||
        card[3] == card[4] && card[4] == card[0] && card[0] == card[1] || card[4] == card[0] && card[0] == card[1] && card[1] == card[2])
        {
            std::cout << "*Four of a kind*" << std::endl;
        }
        // 4.Full house definitions
        else if(card[0] == card[1] && card[2] == card[3] == card[4] || card[3] == card[4] && card[0] == card[1] == card[2])
        {
            std::cout << "*Full house*" << std::endl;
        }
        // 5.Flush definitions
        else if(suit[0] == suit[1] && suit[1]==suit[2] && suit[2]==suit[3] && suit[3]==suit[4])
        {
            std::cout << "*Flush*" << std::endl;
        }
        // 6.Straight definitions
        else if(card[0]==card[1]-2 && card[1]==card[2]-2 && card[2]==card[3]-2 && card[3]==card[4]-2)
        {
            std::cout << "*Straight*" << std::endl;
        }
        // 7.Three of a kind definitions
        else if(card[0] == card[1] && card[1] == card[2] || card[1] == card[2] && card[2] == card[3] || card[2] == card[3] && card[3] == card[4] || card[3] == card[4] && card[4] == card[0] || card[4] == card[0] && card[0] == card[1])
        {
            std::cout << "*Three of a kind*" << std::endl;
        }
        // 8.Two pairs definitions
        else if(card[0] == card[1] && card[2] == card[3] || card[1] == card[2] && card[3] == card[4] || card[2] == card[3] && card[4] == card[0] || card[3] == card[4] && card[0] == card[1] || card[4] == card[0] && card[1] == card[2] || card[0] == card[1] && card[3] == card[4])
        {
            std::cout << "*Two pairs*" << std::endl;
        }
        // 9.One pair definitions
        else if(card[0] == card[1] || card[1]==card[2] || card[2]==card[3] || card[3]==card[4])
        {
            std::cout << "*One pair*" << std::endl;
        }
        // Highest card definitions
        else
        {
            std::cout << "*Highest card is " << card_values[card[4]] << "*" << std::endl;
        }
    
    }
    return 0;
}