# Poker hand with 2decks

Gives you a poker hand of 5 cards from combination of 2 decks of standard playing cards

## Requirements

requires gcc

## Usage

$ mkdir build && cd build

$ ../src/main.cpp -o main

$ ./main

## Maintainers

Kalle Nurminen 